<?php
  include("header.php");
  $title = "Programming Books - Book reviews"; 
  echo "<title>".$title."</title>";
?>

        
    
        <div class="main wrapper clearfix">


            <div class="slider-wrap">

                <ul class="bxslider">
                  <li><a href="#"><img src="images/cover1.jpg"></a></li>
                  <li><a href="#"><img src="images/cover2.jpg"></a></li>
                  <li><a href="#"><img src="images/cover3.jpg"></a></li>
                  
                </ul>


            </div>
             

            <div>
                <h3 id="new-books">Нови книги</h3>
            </div>
            <div id="hpage_latest">
                <div class="list">
                    <ul>
                        <li>
                            <a href="html5-book.php"><img src="images/html5-hacks_1.png" class="img-fluid" alt="HTML5 хакове, Съвети и инструменти за създаване на интерактивни уеб приложения"></a>
                        </li>

                        <li>
                            <a href="java-dobri-strani.php"><img src="images/java_good_parts_1.png" class="img-fluid" alt="Java: Добрите страни" /></a>
                        </li>
                    </ul>
                    <ul>
                        <li>
                            <a href="html5-lipsvashto.php"><img src="images/html5_256_1.png" class="img-fluid" alt="HTML5 Липсващото ръководство" /></a>
                        </li>
                        <li class="last">
                            <a href="php-dobri-strani.php"><img src="images/php_goodparts_256_1-min.png" class="img-fluid" alt="PHP Добрите страни" /></a>
                        </li>
                    </ul>
                </div>

                <br class="clear" />
            </div>

            <article>
                <section>
                    <a id="art-img" href="#"><img src="images/sublime-logo.png" alt="" title="Как да настроим и извлечем максимума, който Sublime Text ни предлага" /></a>
                    <h2><a href="#">Как да настроим и извлечем максимума, който Sublime Text ни предлага</a></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sodales urna non odio egestas tempor. Nunc vel vehicula ante. Etiam bibendum iaculis libero, eget molestie nisl pharetra in. In semper consequat est, eu porta velit mollis nec.</p>
                </section>
                <section>
                    <a id="art-img" href="#"><img src="images/ubuntu.png" alt="" title="Основни команди в Linux" /></a>
                    <h2><a href="#">Основни команди в Linux.</a></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sodales urna non odio egestas tempor. Nunc vel vehicula ante. Etiam bibendum iaculis libero, eget molestie nisl pharetra in. In semper consequat est, eu porta velit mollis nec. Curabitur posuere enim eget turpis feugiat tempor. Etiam ullamcorper lorem dapibus velit suscipit ultrices. Proin in est sed erat facilisis pharetra.</p>
                </section>
                <section>
                    <a id="art-img" href="#"><img src="images/less-mini.png" alt="Инструмент за писане на LESS"></a>
                    <h2><a href="#">Инструмент за писане на LESS</a></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sodales urna non odio egestas tempor. Nunc vel vehicula ante. Etiam bibendum iaculis libero, eget molestie nisl pharetra in. In semper consequat est, eu porta velit mollis nec. Curabitur posuere enim eget turpis feugiat tempor. Etiam ullamcorper lorem dapibus velit suscipit ultrices. Proin in est sed erat facilisis pharetra.</p>
                </section>

            </article>

            <aside>
                <h3>aside</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sodales urna non odio egestas tempor. Nunc vel vehicula ante. Etiam bibendum iaculis libero, eget molestie nisl pharetra in. In semper consequat est, eu porta velit mollis nec. Curabitur posuere enim eget turpis feugiat tempor. Etiam ullamcorper lorem dapibus velit suscipit ultrices.</p>
            </aside>

        </div> <!-- #main -->
    </div> <!-- #main-container -->

<?php 
  include("footer.php");
?>

