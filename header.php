<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->
<head>
    <style>
    .slider-wrap {max-width:960px; margin:0 auto; padding-top:50px}
    
    .bxslider {margin-top:0}
    .bxslider li {left:0}
</style>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Programming books</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="icon-web.png?v=">
    <link rel="stylesheet" href="css/bootstrap.min.css">
   
    <link rel="stylesheet" href="css/normalize.min.css">
    <link rel="stylesheet" href="css/main.css?v=">
    <link rel="stylesheet" href="css/lightbox.css">
    <script type="text/javascript" src="js/vendor/lightbox.min.js"></script>
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script src="js/vendor/jquery-3.1.1.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    <link href="jquery.bxslider.css" rel="stylesheet" />

    



</head>
<body>





    <div class="main-container">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div id="main-pic">
            <a href="./">
            <img src="images/logo-web.png" class="img-fluid" alt="">
            </a>
        </div>
        <div id="custom-bootstrap-menu" class="navbar navbar-default navbar-static-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.php">IT BOOKS</a>
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>

                    </button>
                </div>
                <div class="collapse navbar-collapse navbar-menubuilder">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="index.php">Начало</a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Програмиране <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">C#</a></li>
                                <li><a href="#">Java</a></li>
                                <li><a href="#">C / C++</a></li>
                                <li><a href="#">Perl</a></li>

                            </ul>

                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Операционни системи <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Windows</a></li>
                                <li><a href="#">Linux</a></li>

                            </ul>

                        </li>
                        <li>
                            <a href="#">База данни</a>
                        </li>
                        <li>
                            <a href="#">Уеб дизайн</a>
                        </li>
                        <li>
                            <a href="#">Статии</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
