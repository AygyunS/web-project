<div class="footer-container">

  <!--footer-->
  <footer class="footer1">
    <div class="container">
      <div class="row">
        <!-- row -->

        <div class="col-lg-3 col-md-3">
          <!-- widgets column left -->
          <ul class="list-unstyled clear-margins">
            <!-- widgets -->

            <li class="widget-container widget_nav_menu">
              <!-- widgets list -->

              <h1 class="title-widget">Useful links</h1>

              <ul>
                <li>
                  <a href="#">
                    <i class="fa fa-angle-double-right"></i> Програмиране
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="fa fa-angle-double-right"></i> Операционни системи
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="fa fa-angle-double-right"></i> База данни
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="fa fa-angle-double-right"></i>Уеб дизайн
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="fa fa-angle-double-right"></i>Статии
                  </a>
                </li>

              </ul>

            </li>

          </ul>
        </div>
        <!-- widgets column left end -->
        <div class="col-lg-3 col-md-3">
          <!-- widgets column center -->

          <ul class="list-unstyled clear-margins">
            <!-- widgets -->
            <li class="widget-container widget_recent_news">
              <!-- widgets list -->
              <h1 class="title-widget">Contact Detail </h1>
              <div class="footerp">
                <h2 class="title-median">Webenlance Pvt. Ltd.</h2>
                <p>
                  <b>Email id:</b>
                  <a href="mailto:info@webenlance.com">info@webenlance.com</a>
                </p>
                <p>
                  <b>Helpline Numbers </b>
                  <b style="color:#ffc106;">(8AM to 10PM):</b>  +91-8130890090, +91-8130190010
                </p>

                <p>
                  <b>Corp Office / Postal Address</b>
                </p>
                <p>
                  <b>Phone Numbers : </b>7042827160,
                </p>
                <p> 011-27568832, 9868387223</p>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
  <!--header-->
  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="copyright">
            © 2015, Webenlance, All rights reserved

          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="design">
            <a href="#">Franchisee </a> |  <a target="_blank" href="http://www.webenlance.com">Web Design & Development by Webenlance</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="js/vendor/jquery-1.11.2.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/vendor/lightbox.min.js"></script>
<script type="text/javascript" src="js/vendor/book-tab.js"></script>
<script src="http://bxslider.com/lib/jquery.bxslider.js"></script>



<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<!--<script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X','auto');ga('send','pageview');
    </script>-->
</body>
</html>